public class SobelApp {

	public static void main(String[] args) throws Exception {

		PPMHandler handler = new PPMHandler();
		System.out.println("Loading image.\n");
		Integer[] inImage = handler.loadImage("src/lena.ppm", true);
		
		System.out.println("Running Sobel edge detection.\n");
		Integer[] outImage = handler.detectEdges(handler.getWidth() * handler.getHeight(), inImage);
		
		System.out.println("Saving image.\n");
		handler.writeImage("src/lena_sobel.ppm", outImage, true);
		
		System.out.println("Exiting\n");
	}
}
