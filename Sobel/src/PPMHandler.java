import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;

import com.google.common.io.Files;

public class PPMHandler {
	
	private Integer width;
	private Integer height;

	public Integer[] loadImage(String filepath, Boolean grayscale) throws IOException {
		
		File file = null;
		Integer[] result = null;
		
		try {
			file = new File(filepath);
		}
		catch (Exception e) {
			System.out.printf("Error opening file %s.", filepath);
			e.printStackTrace();
			System.exit(1);
		}
		
		List<String> lines = Files.readLines(file, Charset.defaultCharset());
		Iterator<String> it = lines.iterator();
		
		String type = it.next();
		String[] dimensions = it.next().split(" ");
		Integer width = Integer.parseInt(dimensions[0]);
		Integer height = Integer.parseInt(dimensions[1]);
		String maxIntensity = it.next();
		
		setWidth(width);
		setHeight(height);
		
		result = new Integer[width*height];
		
		Integer pixelsRead = 0;
		Integer pixel = 0;
		Integer currComponent = 0;
		
		for (int i = 0; i < (width * height / 4); i++) {
			String[] readLine = it.next().split(" ");
			int got = readLine.length;

			if (got == 0) {
				System.out.printf("Unexpected end of file after reading %d color values", i);
				System.exit(1);
			}
			
			Integer[] v = new Integer[readLine.length];
			for(int j = 0; j < v.length; j++) {
				v[j] = Integer.parseInt(readLine[j]);
				
				switch (currComponent) {
				case 0:
					pixel = v[j];
					currComponent++;
					break;
				case 1:
					pixel = (pixel << 8) | (v[j] & 0xFF);
					currComponent++;
					break;
				case 2:
					pixel = (pixel << 8) | (v[j] & 0xFF);
					currComponent = 0;
					if (grayscale)
						pixel = ((pixel >> 16) & 0xff) * 3 / 10 + ((pixel >> 8) & 0xff)
								* 59 / 100 + ((pixel) & 0xff) * 11 / 100;
					result[pixelsRead] = pixel;
					pixelsRead++;
					break;
				}
			}
		}
		
		return result;
	}
	
	public void writeImage(String filename, Integer[] result, Boolean grayscale) throws Exception {
		BufferedWriter out = new BufferedWriter(new FileWriter(filename));
		writeLine(out, "P3");
		writeLine(out, "" + getWidth() + " " + getHeight());
		writeLine(out, "255");
				
		for (int i = 0; i < (getWidth()-2)*( getHeight()-2); i++) {
			if (grayscale) {
				result[i] = result[i] > 255 ? 255 : result[i];
				result[i] = result[i] < 0 ? 0 : result[i];
				
				for (int j = 0; j < 3; j++)
					writeLine(out, String.valueOf((Integer)result[i]));
			} else {
				writeLine(out, String.valueOf(((Integer)result[i]) >> 16));
				writeLine(out, String.valueOf((((Integer)result[i]) >> 8) & 0xff));
				writeLine(out, String.valueOf((((Integer)result[i])) & 0xff));
			}
		}
		
		out.close();
	}
	
	private void writeLine(BufferedWriter out, String line) throws Exception {
        out.write(line, 0, line.length());
        out.newLine();
    }
	
	public Integer[] detectEdges(Integer dimensions, Integer[] inImage) {
		
		Integer[] outImage = new Integer[256*256];
		
		for (int i=0; i<outImage.length; i++) {
			outImage[i] = 0;
		}
		
		int height = 256, width = 256;
		
		int[][] Gx = { { -1, 0, 1 }, // y, x
			       { -2, 0, 2 },
			       { -1, 0, 1 } };
		int[][] Gy = { { 1, 2, 1 }, // y, x
			       { 0, 0, 0 },
			       { -1, -2, -1 } };
		
		Integer[][] window = new Integer[3][3];
		
		for (int i = 0; i < width -2; i++) 
		{
			for (int j = 0; j < height -2; j++) 
			{
				if (i == 0 || i == width - 2 || j == 0 || j == height - 2)
				{
					outImage[(i+1)*(width) + (j+1)] = 0;
				}
				
				for (int x = 0; x <= 2; x++)
				{
					for (int y = 0; y <= 2; y++)
					{
						window[x][y] = (inImage[(y+j) * width + (x+i)]);			 
					}
				}
				
				Integer GxSum = 0;
				Integer GySum = 0;
				
				for (int x = 0; x < 3; x++) {
					for (int y = 0; y < 3; y++) {
						GxSum = GxSum + window[x][y] * Gx[y][x];
						GySum = GySum + window[x][y] * Gy[y][x];
					}
				}

				Integer absGxSum = GxSum >= 0 ? GxSum : -GxSum;
				Integer absGySum = GySum >= 0 ? GySum : -GySum;

				Integer result = absGxSum + absGySum;

				outImage[(j+1)*(width) + (i+1)] = result;
			}
		}
		
		
		return outImage;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}
}
